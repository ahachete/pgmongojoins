# Comparison of JOINS in MongoDB and PostgreSQL

This repository contains the source code for all the queries used to
exemplify the use cases described on the blog post "**Comparison of
JOINS in MongoDB and PostgreSQL**". This post has been authored by Dr.
Michael Stonebraker and [Álvaro Hernández](https://aht.es), and has been
published on [EnterpriseDB's blog website](https://www.enterprisedb.com/blog/comparison-joins-mongodb-vs-postgresql).

This repository is structured in the following folders:
*  `data_loader`. Contains the source code of a program used to perform
   the benchmark detailed in the blog post.
*  `ddl`. Contains the database structured for PostgreSQL. Also a helper
   script to easily truncate existing data.
*  `post-examples`. Full code used in the blog post, ready for easy c&p
   and reproduce in your own environment.
