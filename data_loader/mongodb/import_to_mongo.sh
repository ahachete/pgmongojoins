#!/bin/bash
# Requirements: jq parallel

TEMPDIR="/mnt/temp"
SQL_M1_DEPARTMENTS="select row_to_json(r) from (select dname, floor, budget from model1.department) r"
SQL_M1_EMPLOYEE="select row_to_json(r) from (select ename,age,salary,department from model1.employee) r"
SQL_M2_DEPARTMENTS="select row_to_json(r) from (select dname, floor, budget from model2.department) r"
SQL_M2_EMPLOYEE="
    WITH getObj AS (
SELECT ename, age, salary,
json_build_object('dname',dname,'dedication_pct', dedication_pct) dept
FROM model2.employee
left outer join model2.works_in using (ename)
group by ename,dname,dedication_pct
),
arrays AS (
SELECT getObj.ename, getObj.age, getObj.salary,
array_agg(dept) arr
FROM getObj
group by getObj.ename, getObj.age, getObj.salary
)
SELECT row_to_json(r)
FROM
    (
       SELECT arrays.ename, arrays.age, arrays.salary, array_to_json(arrays.arr) departments
       from arrays

    ) r
"

function mongoimp {
  IMPORT_MONGO="mongoimport --jsonArray --db ${1} --collection ${2}"
  #IMPORT_MONGO="cat"
  echo ${IMPORT_MONGO}
}

function psqlexp {
    # no readline, tuple only
    CMD="psql -nt ${@}"
    echo $CMD
}

function export_import {
  db=$1
  col=$2
  outf="${TEMPDIR}/temp.out"
  shift ; shift
  # We use heredoc due to multilined queries
  $(psqlexp -o ${outf} ${db}) <<QLEOF
  COPY (${@}) TO STDOUT
QLEOF
  split -n l/20 ${outf} ${outf}_
  echo "Sanity Check $(wc -l ${outf})"
  ls ${outf}_* | parallel -j3 "jq -s '.' {} | $(mongoimp ${db} ${col})"
  rm -f ${outf}* 2>/dev/null
}

mkdir ${TEMPDIR} 2> /dev/null

case ${1} in
  "m1")
    echo $(export_import pgmongojoins m1_departments ${SQL_M1_DEPARTMENTS})
    echo $(export_import pgmongojoins m1_employee ${SQL_M1_EMPLOYEE})
    ;;
  "m2")
    echo $(export_import pgmongojoins m2_departments ${SQL_M2_DEPARTMENTS})
    echo $(export_import pgmongojoins m2_employee ${SQL_M2_EMPLOYEE})
    ;;
  "*")
    echo "Not implemented"
    ;;
esac

