package es.aht.pgmongojoins.util;


import java.sql.SQLException;


@FunctionalInterface
public interface SQLExceptionSupplier<T> {
    T get() throws SQLException;
}
