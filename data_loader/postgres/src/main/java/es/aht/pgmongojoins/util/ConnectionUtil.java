package es.aht.pgmongojoins.util;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.function.Supplier;


public class ConnectionUtil {
    private static final String JDBC_URL_PREFIX = "jdbc:postgresql://";

    private final PropertiesConfiguration propertiesConfiguration;

    public ConnectionUtil(Properties properties) {
        this.propertiesConfiguration = new PropertiesConfiguration(properties);
    }

    public SQLExceptionSupplier<Connection> connectionSupplier() throws SQLException {
        return () -> newConnection();
    }

    public Connection newConnection() throws SQLException {
        return DriverManager.getConnection(
                getUrlFromProperties(), propertiesConfiguration.user(), propertiesConfiguration.password()
        );
    }

    private String getUrlFromProperties() {
        return JDBC_URL_PREFIX
                + propertiesConfiguration.host()
                + ":" + propertiesConfiguration.port()
                + "/" + propertiesConfiguration.database()
                ;
    }
}
