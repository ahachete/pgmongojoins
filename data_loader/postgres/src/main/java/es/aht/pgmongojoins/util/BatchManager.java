package es.aht.pgmongojoins.util;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class BatchManager {
    private final int size;
    private final Connection connection;
    private final PreparedStatement preparedStatement;
    private int batchNumber = 0;

    public BatchManager(int size, Connection connection, PreparedStatement preparedStatement) {
        this.size = size;
        this.connection = connection;
        this.preparedStatement = preparedStatement;
    }

    public void submit() throws SQLException {
        preparedStatement.addBatch();
        if(++batchNumber == size) {
            executeBatchCommit();
            batchNumber = 0;
        }
    }

    public void finish() throws SQLException {
        executeBatchCommit();
    }

    private void executeBatchCommit() throws SQLException {
        preparedStatement.executeBatch();
        connection.commit();
    }

    public static void doWithBatchManager(
            Connection connection, PreparedStatement preparedStatement, int batchSize,
            SQLExceptionConsumer<BatchManager> consumer
    ) throws SQLException {
        var batchManager = new BatchManager(batchSize, connection, preparedStatement);
        consumer.accept(batchManager);
        batchManager.finish();
    }
}
