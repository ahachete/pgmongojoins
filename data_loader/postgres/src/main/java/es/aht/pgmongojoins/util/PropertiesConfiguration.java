package es.aht.pgmongojoins.util;


import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


public class PropertiesConfiguration {
    private static final String PROPERTIES_HOST_KEY = "host";
    private static final String PROPERTIES_PORT_KEY = "port";
    private static final String PROPERTIES_DBNAME_KEY = "dbname";
    private static final String PROPERTIES_USER_KEY = "user";
    private static final String PROPERTIES_PASSWORD_KEY = "password";
    private static final String PROPERTIES_DEPARTMENTS_KEY = "departments";
    private static final String PROPERTIES_EMPLOYEES_KEY = "employees";
    private static final String PROPERTIES_MAXWORKSINDEPTS_KEY = "max_works_in_depts";
    private static final String PROPERTIES_LOADER_THREADS = "loader_threads";

    private static final Map<String,String> PROPERTIES_NAMES_DEFAULT_VALUES = new HashMap<>();
    static {
        PROPERTIES_NAMES_DEFAULT_VALUES.put(PROPERTIES_HOST_KEY, "localhost");
        PROPERTIES_NAMES_DEFAULT_VALUES.put(PROPERTIES_PORT_KEY, "5432");
        PROPERTIES_NAMES_DEFAULT_VALUES.put(PROPERTIES_DBNAME_KEY, "pgmongojoins");
        PROPERTIES_NAMES_DEFAULT_VALUES.put(PROPERTIES_USER_KEY, "pgmongojoins");
        PROPERTIES_NAMES_DEFAULT_VALUES.put(PROPERTIES_PASSWORD_KEY, "");
        PROPERTIES_NAMES_DEFAULT_VALUES.put(PROPERTIES_DEPARTMENTS_KEY, "100");
        PROPERTIES_NAMES_DEFAULT_VALUES.put(PROPERTIES_EMPLOYEES_KEY, "10_000");
        PROPERTIES_NAMES_DEFAULT_VALUES.put(PROPERTIES_MAXWORKSINDEPTS_KEY, "5");
        PROPERTIES_NAMES_DEFAULT_VALUES.put(PROPERTIES_LOADER_THREADS, "1");
    }

    private final Properties properties;

    public PropertiesConfiguration(Properties properties) {
        this.properties = properties;
    }

    private String getPropertiesOrDefaultValue(String key) {
        return properties.getProperty(key, PROPERTIES_NAMES_DEFAULT_VALUES.get(key));
    }

    private int getPropertiesOrDefaultValueAsInt(String key) {
        return Integer.parseInt(getPropertiesOrDefaultValue(key));
    }

    public String host() {
        return getPropertiesOrDefaultValue(PROPERTIES_HOST_KEY);
    }

    public String port() {
        return getPropertiesOrDefaultValue(PROPERTIES_PORT_KEY);
    }

    public String database() {
        return getPropertiesOrDefaultValue(PROPERTIES_DBNAME_KEY);
    }

    public String user() {
        return getPropertiesOrDefaultValue(PROPERTIES_USER_KEY);
    }

    public String password() {
        return getPropertiesOrDefaultValue(PROPERTIES_PASSWORD_KEY);
    }

    public int departments() {
        return getPropertiesOrDefaultValueAsInt(PROPERTIES_DEPARTMENTS_KEY);
    }

    public int employees() {
        return getPropertiesOrDefaultValueAsInt(PROPERTIES_EMPLOYEES_KEY);
    }

    public int maxWorkInDepartments() {
        return getPropertiesOrDefaultValueAsInt(PROPERTIES_MAXWORKSINDEPTS_KEY);
    }

    public int threads() {
        return getPropertiesOrDefaultValueAsInt(PROPERTIES_LOADER_THREADS);
    }
}
