package es.aht.pgmongojoins;


import es.aht.pgmongojoins.operation.AbstractDataLoader;
import es.aht.pgmongojoins.operation.Model1DataLoader;
import es.aht.pgmongojoins.operation.Model2DataLoader;
import es.aht.pgmongojoins.util.ConnectionUtil;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;


public class Main {
    public static void main(String[] args) {
        validateArgs(args);

        var properties = new Properties();
        try(var propertiesReader = new FileReader(args[1])) {
            properties.load(propertiesReader);
        } catch (FileNotFoundException e) {
            errorAndExit("Properties file not found", 2);
        } catch (IOException e) {
            throw new RuntimeException("IOException reading properties file", e);
        }

        try {
            var connectionUtil = new ConnectionUtil(properties);

            AbstractDataLoader dataLoader = null;
            switch (args[0]) {
                case "loader1":
                    dataLoader = new Model1DataLoader(properties, connectionUtil.connectionSupplier());
                    break;
                case "loader2":
                    dataLoader = new Model2DataLoader(properties, connectionUtil.connectionSupplier());
                    break;
                default:
                    errorAndExit("Invalid loader mode", 4);
            }

            dataLoader.generateData();
        } catch (SQLException e) {
            throw new RuntimeException("Programming error", e);
        }
    }

    private static void errorAndExit(String error, int exitCode) {
        System.err.println(error);
        System.exit(exitCode);
    }

    private static void validateArgs(String[] args) {
        if(args.length != 2) {
            errorAndExit("Args: (loader1|loader2) $properties_file", 1);
        }
    }
}
