db.employee.insert([
  {
    "_id": "1",
    "ename": "Bill",
    "age": 36,
    "salary": 10000,
    "departments": [
      { "dept": "1002", "dedication_pct": 60 },
      { "dept": "1001", "dedication_pct": 40 }
    ]
  },
  {
    "_id": "2",
    "ename": "Sam",
    "age": 27,
    "salary": 15000,
    "departments": [
      { "dept": "1002", "dedication_pct": 100 }
    ]
  },
  {
    "_id": "3",
    "ename": "Fred",
    "age": 29,
    "salary": 12000,
    "departments": [
      { "dept": "1001", "dedication_pct": 100 }
    ]
  }
]);


db.department.insert([
  {
    "_id": "1001",
    "dname": "Shoe",
    "floor": 1,
    "budget": 1200
  },
  {
    "_id": "1002",
    "dname": "Toy",
    "floor": 2,
    "budget": 1400
  },
  {
    "_id": "1003",
    "dname": "Candy",
    "floor": 1,
    "budget": 900
  }
]);


db.employee.aggregate([
  {
    $unwind: "$departments"
  },
  {
    $project: {
      "_id": 0,
      "salary": 1,
      "department": "$departments.dept",
      "dedication_pct": "$departments.dedication_pct"
    }
  },
  {
    $lookup:
    {
      from: "department",
      localField: "department",
      foreignField: "_id",
      as: "dept"
    }
  },
  {
    $unwind: "$dept"
  },

  {
    $group:
    {
      _id: "$dept.dname",
      totalsalary: { $sum: { $multiply: [ "$salary", "$dedication_pct", 0.01 ] } }
    }
  }
]);

{ "_id" : "Shoe", "totalsalary" : 16000 }
{ "_id" : "Toy", "totalsalary" : 21000 }
